package com.example.denywenk.kuliah;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class login extends AppCompatActivity {
    public EditText Username, Password;
    public Button Btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Username = findViewById(R.id.username);
        Password = findViewById(R.id.password);
        Btn = findViewById(R.id.btn);

        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validasi(Username.getText().toString(), Password.getText().toString());
            }
        });

    }

    private void validasi(String userName, String pasSword){
        if((userName.equals("admin")) && (pasSword.equals("stts"))){

            Intent intent = new Intent(login.this, masuk.class);
            startActivity(intent);
        } else if((userName.equals("")) && (pasSword.equals(""))){
            Toast.makeText(this,"username and password tidak boleh kosong!",Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(this,"passwordnya harus 'stts'",Toast.LENGTH_LONG).show();
        }
    }

}
